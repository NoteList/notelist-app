package com.example.notelist_app.data.model

data class Lista(val id: Int, val nombre: String, var tareaArrayList: ArrayList<Tarea>)
