package com.example.notelist_app.data.model

data class Tarea(val idTar: Int, val idLista: Int, val description: String, var done: Boolean)