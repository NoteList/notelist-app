package com.example.notelist_app.data.api

import com.example.notelist_app.data.model.Lista
import com.example.notelist_app.data.model.Tarea
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*


interface ApiToDoList {

    @GET("listas")
    fun getllista(): Call<List<Lista>>

    @GET("listas/{id}/tareas")
    fun gettarea(@Path("id")id : Int): Call<List<Tarea>>

    @POST("listas")
    fun addLista(@Body objetoListas: Lista): Call<Lista>

    @POST("listas/{id}/tareas")
    fun addTarea(@Path("id")id : Int , @Body objetoTarea: Tarea): Call<Tarea>
    
    @PUT("listas/{id}/tareas/{idTar}")
    fun putTarea(@Path("id")id : Int, @Path("idTar") idTar: Int, @Body objetoTarea: Tarea): Call<Tarea>

    @DELETE("listas/{id}")
    fun deleteLista(@Path("id") id: Int): Call<Lista>

    @DELETE("listas/{id}/tareas/{idTar}")
    fun deleteTarea(@Path("id") id: Int,@Path("idTar") idTar: Int): Call<Tarea>



    companion object {
        val BASE_URL = "https://notelistclan.herokuapp.com/"
        fun create(): ApiToDoList {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(ApiToDoList::class.java)
        }
    }
}