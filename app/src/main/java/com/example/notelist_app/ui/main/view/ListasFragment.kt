package com.example.notelist_app.ui.main.view

import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.notelist_app.R
import com.example.notelist_app.ui.main.viewModel.TaskViewModel
import com.example.notelist_app.data.model.Lista
import com.example.notelist_app.ui.main.adapter.ListaAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton

class ListasFragment: Fragment(R.layout.lists_layout) {

    private lateinit var recyclerView: RecyclerView
    private val viewModel: TaskViewModel by activityViewModels()

    private lateinit var listTitle: TextView
    private lateinit var addListButton: FloatingActionButton

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView = view.findViewById(R.id.recycler_view)

        listTitle = view.findViewById(R.id.list_t)
        addListButton = view.findViewById(R.id.add_list)

        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = ListaAdapter(viewModel.getLists(),::deleteLista)

        if (viewModel.ft == 1){
            recyclerView.adapter = ListaAdapter(viewModel.getLists(),::deleteLista)
        }else {
            Handler().postDelayed({
                recyclerView.adapter = ListaAdapter(viewModel.getLists(),::deleteLista)
                viewModel.ft = 1
            }, 1000)
        }

        Handler().postDelayed({
            recyclerView.adapter = ListaAdapter(viewModel.getLists(),::deleteLista)
        }, 1000)

        addListButton.setOnClickListener {
            findNavController().navigate(R.id.action_listasFragment_to_addListFragment)
        }
    }
    private fun deleteLista(lista: Lista) {
        viewModel.deleteListaApi(lista.id)
        viewModel.deleteLista(lista)
    }
}