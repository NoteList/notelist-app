package com.example.notelist_app.ui.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.notelist_app.R
import com.example.notelist_app.data.model.Lista
import com.example.notelist_app.ui.main.view.ListasFragmentDirections
import com.example.notelist_app.ui.main.viewModel.TaskViewModel

class ListaAdapter(listas: List<Lista>, private val onDeleteCallback: (Lista) -> Unit):  RecyclerView.Adapter<ListaAdapter.ListaViewHolder>() {

    private var listas: MutableList<Lista> = listas as MutableList<Lista>


    inner class ListaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var nomLista: TextView
        private lateinit var tanca : ImageView

        init {
            nomLista = itemView.findViewById(R.id.nom_lista)
        }

        fun bindData(lista: Lista, index: Int) {
            nomLista.text = lista.nombre

            tanca = itemView.findViewById(R.id.tanca)
            tanca.setOnClickListener {deleteLista(lista, index)}
        }
    }
    fun deleteLista(lista: Lista, index: Int){
        var viewmodel = TaskViewModel();
        for (i in lista.tareaArrayList) {
            viewmodel.deleteTarea(i)
        }
        viewmodel.deleteLista(lista)
        onDeleteCallback(lista)
        listas.remove(lista)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListaViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.lista_item, parent, false)
        return ListaViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListaViewHolder, position: Int) {
        holder.bindData(listas[position], position)

        holder.itemView.setOnClickListener {
            val directions = ListasFragmentDirections.actionListasFragmentToTareasFragment(listas[position].id)
            Navigation.findNavController(it).navigate(directions)
        }

    }

    override fun getItemCount(): Int {
        return listas.size
    }
}