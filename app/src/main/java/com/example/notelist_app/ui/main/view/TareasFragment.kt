package com.example.notelist_app.ui.main.view

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.notelist_app.R
import com.example.notelist_app.ui.main.viewModel.TaskViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.example.notelist_app.data.model.Tarea
import com.example.notelist_app.ui.main.adapter.TareaAdapter


class TareasFragment : Fragment(R.layout.tasks_layout){
    private lateinit var recyclerView: RecyclerView
    private val viewModel: TaskViewModel by activityViewModels()

    private lateinit var atras: FloatingActionButton
    private lateinit var addButton: FloatingActionButton


    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)

        val idLista = arguments?.getInt("idLista")

        recyclerView = view.findViewById(R.id.recycler_view)
        addButton = view.findViewById(R.id.add_task)
        atras = view.findViewById(R.id.atras)

        recyclerView.layoutManager = GridLayoutManager(requireContext(),2)
        recyclerView.adapter = idLista?.let { viewModel.getTareasId(it) }?.let { TareaAdapter(it) }


        if (viewModel.ft == 1){
            recyclerView.adapter = idLista?.let { viewModel.getTareasId(it) }?.let { TareaAdapter(it) }
        }else {
            Handler().postDelayed({
                recyclerView.adapter = idLista?.let { viewModel.getTareasId(it) }?.let { TareaAdapter(it) }
                viewModel.ft = 1
            }, 1000)
        }

        Handler().postDelayed({
            recyclerView.adapter = idLista?.let { viewModel.getTareasId(it) }?.let { TareaAdapter(it) }
        }, 1000)

        addButton.setOnClickListener {
            val directions = arguments?.let { it1 ->
                TareasFragmentDirections.actionTareasFragmentToAddTareaFragment(it1?.getInt("idLista"))
            }
            if (directions != null) {
                Navigation.findNavController(it).navigate(directions)
            }
        }


        atras.setOnClickListener {
//            for (i in viewModel.getTareas()){
//                for (j in i.tareaArrayList) {
//                    Log.w("T", j.toString())
//                    viewModel.putTareaApi(j)
//                }
//            }
            findNavController().navigate(R.id.action_tareasFragment_to_listasFragment)
        }
    }

    private fun deleteTarea(tarea: Tarea) {
        viewModel.deleteTareaApi(tarea.idLista, tarea.idTar)
//        viewModel.deleteTarea(tarea)
    }
}