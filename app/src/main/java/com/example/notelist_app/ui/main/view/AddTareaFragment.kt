package com.example.notelist_app.ui.main.view

import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.example.notelist_app.R
import com.example.notelist_app.ui.main.viewModel.TaskViewModel
import com.example.notelist_app.data.model.Tarea
import com.google.android.material.textfield.TextInputLayout

class AddTareaFragment : Fragment(R.layout.tarea_fragment){
    private val viewModel: TaskViewModel by activityViewModels()

    private lateinit var tituloNombre: TextView
    private lateinit var inputText: TextInputLayout
    private lateinit var inputTextTask: EditText
    private lateinit var done: CheckBox
    private lateinit var addTareaButton: Button

    private var task: Tarea? = null
    private var taskPosition: Int? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        if (arguments?.isEmpty != true) {
//            taskPosition = arguments!!.get("taskPosition") as Int
//            task = viewModel.getTareas()[taskPosition!!]
//        }

        tituloNombre = view.findViewById(R.id.titleName)
        inputText = view.findViewById(R.id.name)
        inputTextTask = view.findViewById(R.id.taskText)
        addTareaButton = view.findViewById(R.id.addTarea)

        val text = "Introduce una tarea"
        val duration = Toast.LENGTH_SHORT
        val toast = Toast.makeText(requireContext(), text, duration)

        var passarIdLista = arguments?.getInt("idLista")

        if (task != null) {
            addTareaButton.text = "Update"
            inputTextTask.setText(task!!.description)
        }

        addTareaButton.setOnClickListener {
            val value = inputTextTask.text.toString()
            val posTarea = viewModel.getTareas().size + 1

            if (value == ""){
                toast.show()
            }
            if (checkFields()) {
                task = arguments?.getInt("idLista")?.let { it1 ->
                    Tarea(
                        posTarea,
                        it1,
                        inputTextTask.text.toString(),
                         false
                    )
                }
                if (addTareaButton.text.equals("ADD")) {
                    viewModel.addTarea(task!!)
                    viewModel.addTareaPost(task!!)
                }
                else{
                    viewModel.updateTareas(taskPosition!!, task!!)
                }
                val action = passarIdLista?.let { it1 ->
                    AddTareaFragmentDirections.actionAddTareaFragmentToTareasFragment(
                        it1
                    )
                }
                if (action != null) {
                    Navigation.findNavController(it).navigate(action)
                }
            }
        }
    }
    private fun checkFields(): Boolean {
        if (inputTextTask.text.toString() == "") inputText.error = getString(R.string.error_message)
        return inputTextTask.text.toString() != ""
    }
}