package com.example.notelist_app.ui.main.adapter


import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModel
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.notelist_app.R
import com.example.notelist_app.data.model.Tarea
import com.example.notelist_app.ui.main.view.ListasFragmentDirections
import com.example.notelist_app.ui.main.viewModel.TaskViewModel
import java.util.ArrayList

class TareaAdapter(private var tareas: ArrayList<Tarea>) : RecyclerView.Adapter<TareaAdapter.TareaViewHolder>() {

    inner class TareaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var nomTarea: TextView
        private lateinit var cerrar : ImageView
        private lateinit var posit : ImageView



        init {
            nomTarea = itemView.findViewById(R.id.nomTask)
        }

        fun bindData(tarea: Tarea, index: Int) {
            nomTarea.text = tarea.description
            posit = itemView.findViewById(R.id.fons)

            if (tarea.done){
                posit.setImageResource(R.drawable.fet)
            }else if (!tarea.done){
                posit.setImageResource(R.drawable.posit)
            }

            posit.setOnClickListener {
                if (!tarea.done) {
                    posit.setImageResource(R.drawable.fet)
                    tarea.done = true
                    postTarea(tarea)
                } else if (tarea.done) {
                    posit.setImageResource(R.drawable.posit)
                    tarea.done = false
                    postTarea(tarea)
                }
            }

            cerrar = itemView.findViewById(R.id.cerrar)
            cerrar.setOnClickListener {deleteTarea(index)}
        }
    }

    fun postTarea(tarea: Tarea){
        var viewModel = TaskViewModel()
        viewModel.putTareaApi(tarea)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun deleteTarea(index: Int){
        var viewmodel = TaskViewModel();
        var tarea = tareas[index]
        viewmodel.deleteTarea(tarea)
        tareas.removeAt(index)
        notifyDataSetChanged()
        Log.d("rererre", tareas.toString())
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TareaViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.task_item, parent, false)
        return TareaViewHolder(view)
    }

    override fun onBindViewHolder(holder: TareaViewHolder, position: Int) {
        holder.bindData(tareas[position], position)
    }

    override fun getItemCount(): Int {
        return tareas.size
    }
}
