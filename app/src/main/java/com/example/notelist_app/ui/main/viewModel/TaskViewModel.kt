package com.example.notelist_app.ui.main.viewModel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import com.example.notelist_app.data.api.ApiToDoList
import com.example.notelist_app.data.model.Lista
import com.example.notelist_app.data.model.Tarea
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class TaskViewModel (): ViewModel(){
    private var listas = mutableListOf<Lista>()
    private var tareas = mutableListOf<Tarea>()

    init {
        loadLista()
        for (lista in listas){
            if (lista.tareaArrayList.isNotEmpty()){
                loadTarea(lista.id)
            }
        }
    }

    fun loadLista(){
        val call = ApiToDoList.create().getllista()
        call.enqueue(object: Callback<List<Lista>> {
            override fun onFailure(call: Call<List<Lista>>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
            }
            override fun onResponse(call: Call<List<Lista>>, response: Response<List<Lista>?>) {
                if (response.isSuccessful) {
                    listas = response.body() as MutableList<Lista>
                    Log.d("RESPONSE", listas.toString())
                }
            }
        })
    }

    fun addListaPost(list: Lista){
        val call = ApiToDoList.create().addLista(list)
        call.enqueue(object: Callback<Lista> {
            override fun onResponse(call: Call<Lista>, response: Response<Lista>) {

            }

            override fun onFailure(call: Call<Lista>, t: Throwable) {

            }
        })
    }

    fun addTareaPost(tarea: Tarea){
        val call = ApiToDoList.create().addTarea(tarea.idLista, tarea)
        call.enqueue(object: Callback<Tarea> {
            override fun onResponse(call: Call<Tarea>, response: Response<Tarea>) {
            }
            override fun onFailure(call: Call<Tarea>, t: Throwable) {
            }
        })
    }

    fun deleteListaApi(id: Int){
        val call = ApiToDoList.create().deleteLista(id)
        call.enqueue(object: Callback<Lista> {
            override fun onResponse(call: Call<Lista>, response: Response<Lista>) {
                Log.d("ResDEL", response.toString())
            }
            override fun onFailure(call: Call<Lista>, t: Throwable) {
                Log.d("FailDEL", t.toString())
            }
        })
    }



    fun deleteTareaApi(id: Int, idTar: Int){
        val call = ApiToDoList.create().deleteTarea(id, idTar)
        call.enqueue(object: Callback<Tarea> {
            override fun onResponse(call: Call<Tarea>, response: Response<Tarea>) {
                Log.d("ResDEL", response.toString())
            }
            override fun onFailure(call: Call<Tarea>, t: Throwable) {
                Log.d("FailDEL", t.toString())
            }
        })
    }

    fun putTareaApi(tarea: Tarea){
        val call = ApiToDoList.create().putTarea(tarea.idLista, tarea.idTar, tarea)
        call.enqueue(object : Callback<Tarea>{
            override fun onResponse(call: Call<Tarea>, response: Response<Tarea>) {
                Log.d("ResPUT", response.toString())
            }
            override fun onFailure(call: Call<Tarea>, t: Throwable) {
                Log.d("FailPUT", t.toString())

            }
        })
    }

    fun loadTarea(id: Int){
        val call = ApiToDoList.create().gettarea(id)
        call.enqueue(object: Callback<List<Tarea>> {
            override fun onFailure(call: Call<List<Tarea>>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
            }
            override fun onResponse(call: Call<List<Tarea>>, response: Response<List<Tarea>?>) {
                if (response.isSuccessful) {
                    tareas = response.body() as MutableList<Tarea>
                    Log.d("RESPONSE", response.toString())
                }
            }
        })
    }

    fun addList(list: Lista) = listas.add(list)

    var ft = 0
    fun getLists() = listas


    fun updateList(listPosition: Int, list: Lista){
        listas[listPosition] = list
    }
    fun deleteLista(lista: Lista){
        deleteListaApi(lista.id)
        listas.remove(lista)
    }

    fun addTarea(tarea: Tarea) {
        val idLista = tarea.idLista
        for (lista in listas){
            if (lista.id == idLista){
                lista.tareaArrayList += tarea
            }
        }
    }

    var fx = 0
    fun getTareas() = listas

    fun getTareasId(id: Int):ArrayList<Tarea> {
        var listado: ArrayList<Tarea>
        listado = arrayListOf()
        for (lista in listas){
            if (lista.id == id){
                listado = lista.tareaArrayList
            }
        }
        return listado
    }


    fun updateTareas(tareaPosition: Int, tarea: Tarea){
        tareas[tareaPosition] = tarea
    }

    fun deleteTarea(tarea: Tarea) {
        deleteTareaApi(tarea.idLista, tarea.idTar)
        tareas.remove(tarea)
    }
}