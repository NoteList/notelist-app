package com.example.notelist_app.ui.main.view

import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.notelist_app.R
import com.example.notelist_app.ui.main.viewModel.TaskViewModel
import com.example.notelist_app.data.model.Lista
import com.example.notelist_app.data.model.Tarea
import com.google.android.material.textfield.TextInputLayout

class AddListFragment: Fragment(R.layout.lista_fragment) {

    private val viewModel: TaskViewModel by activityViewModels()

    private lateinit var tituloNombre: TextView
    private lateinit var inputText: TextInputLayout
    private lateinit var inputTextList: EditText
    private lateinit var addListButton: Button

    private var list: Lista? = null
    private var listPosition: Int? = null


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tituloNombre = view.findViewById(R.id.tituloNombre)
        inputText = view.findViewById(R.id.nombre)
        inputTextList = view.findViewById(R.id.nameText)
        addListButton = view.findViewById(R.id.addList)
        val text = "Introduce una lista"
        val duration = Toast.LENGTH_SHORT
        val toast = Toast.makeText(requireContext(), text, duration)


        if (list != null) {
            addListButton.text = "Update"
            inputTextList.setText(list!!.nombre)
        }

        addListButton.setOnClickListener {
            val value = inputTextList.text.toString()
            val pos = viewModel.getLists().size + 1
            val tarea = arrayListOf<Tarea>()
            if (value == ""){
                toast.show()
            }
            if (checkFields()) {
                list = Lista(
                    pos,
                    inputTextList.text.toString(),
                    tarea
                )
                if (addListButton.text.equals("ADD")) {
                    viewModel.addList(list!!)
                    viewModel.addListaPost(list!!)
                }
                else viewModel.updateList(listPosition!!, list!!)
                findNavController().navigate(R.id.action_addListFragment_to_listasFragment)
            }
        }
    }
    private fun checkFields(): Boolean {
        if (inputTextList.text.toString() == "") inputText.error = getString(R.string.error_message)
        return inputTextList.text.toString() != ""
    }
}